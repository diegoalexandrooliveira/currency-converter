package tech.jaya.currencyconverter.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.MDC;

import static org.assertj.core.api.Assertions.assertThat;

class LogTraceTest {

    @DisplayName("Should put trace id in MDC")
    @Test
    void traceId() {
        LogTrace.traceLog();
        String tracingId = MDC.get("tracing.id");
        assertThat(tracingId).isNotNull();
    }

}