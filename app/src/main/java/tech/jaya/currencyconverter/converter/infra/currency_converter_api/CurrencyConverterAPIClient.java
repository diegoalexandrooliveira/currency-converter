package tech.jaya.currencyconverter.converter.infra.currency_converter_api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.Map;

@FeignClient(value = "currencyConverterApi", url = "${currencyConverterApi.url}")
public interface CurrencyConverterAPIClient {

    @GetMapping(path = "/convert?apiKey=${currencyConverterApi.key}&compact=ultra")
    Map<String, BigDecimal> convert(@RequestParam(name = "q") String fromTo);

}
