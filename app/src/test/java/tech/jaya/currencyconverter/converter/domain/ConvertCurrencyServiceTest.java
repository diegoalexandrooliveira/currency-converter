package tech.jaya.currencyconverter.converter.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import tech.jaya.currencyconverter.transaction.domain.Currency;
import tech.jaya.currencyconverter.transaction.domain.Transaction;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ConvertCurrencyServiceTest {

    private CurrencyRateService currencyRateService;
    private ConvertCurrencyService convertCurrencyService;

    @BeforeEach
    void setup() {
        currencyRateService = mock(CurrencyRateService.class);
        convertCurrencyService = new ConvertCurrencyService(currencyRateService);
    }

    @DisplayName("Should create a transaction with returned rate")
    @Test
    void createTransaction() {
        ConversionRequest conversionRequest = new ConversionRequest(1L, Currency.BRL, Currency.USD, BigDecimal.ONE);

        when(currencyRateService.getRate(Currency.BRL, Currency.USD)).thenReturn(BigDecimal.valueOf(5.11));

        Transaction transactionCreated = convertCurrencyService.convert(conversionRequest);

        assertThat(transactionCreated)
                .isNotNull()
                .hasFieldOrPropertyWithValue("conversionRate", BigDecimal.valueOf(5.11))
                .hasFieldOrPropertyWithValue("from", Currency.BRL)
                .hasFieldOrPropertyWithValue("to", Currency.USD)
                .hasFieldOrPropertyWithValue("value", BigDecimal.ONE);
    }
}