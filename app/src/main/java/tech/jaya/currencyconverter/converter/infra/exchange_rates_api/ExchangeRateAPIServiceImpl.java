package tech.jaya.currencyconverter.converter.infra.exchange_rates_api;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import tech.jaya.currencyconverter.converter.domain.CurrencyRateService;
import tech.jaya.currencyconverter.transaction.domain.Currency;

import java.math.BigDecimal;

@Service
@RequiredArgsConstructor
@ConditionalOnProperty(name = "exchange-rate-api", havingValue = "exchangeRate")
public class ExchangeRateAPIServiceImpl implements CurrencyRateService {

    private final ExchangeRateAPIClient exchangeRateAPIClient;

    @Override
    public BigDecimal getRate(Currency currencyFrom, Currency currencyTo) {
        var exchangeRateResponse = exchangeRateAPIClient.getLatest(currencyFrom.toString());
        return exchangeRateResponse.getRates().get(currencyTo.toString());
    }
}
