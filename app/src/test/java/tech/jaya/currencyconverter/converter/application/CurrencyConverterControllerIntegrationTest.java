package tech.jaya.currencyconverter.converter.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.integration.ClientAndServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import tech.jaya.currencyconverter.transaction.domain.Currency;
import tech.jaya.currencyconverter.transaction.domain.Transaction;
import tech.jaya.currencyconverter.transaction.domain.TransactionRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.from;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@SpringBootTest(properties = {"currencyConverterApi.url=http://localhost:1080"})
@AutoConfigureMockMvc
class CurrencyConverterControllerIntegrationTest {

    private ClientAndServer clientAndServer;

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TransactionRepository transactionRepository;

    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setup() {
        clientAndServer = ClientAndServer.startClientAndServer(1080);
    }

    @AfterEach
    void cleanUp() {
        clientAndServer.stop();
    }

    @DisplayName("Should convert currency BRL to JPY and save the transaction")
    @Test
    void convertBRLToJPY() throws Exception {
        clientAndServer.reset();
        clientAndServer.when(request()
                .withMethod("GET")
                .withPath("/convert"))
                .respond(response().withContentType(org.mockserver.model.MediaType.APPLICATION_JSON)
                        .withBody("{\"BRL_JPY\":21.434461}"));

        CurrencyConverterRequest currencyConverterRequest = new CurrencyConverterRequest();
        ReflectionTestUtils.setField(currencyConverterRequest, "userId", 1L);
        ReflectionTestUtils.setField(currencyConverterRequest, "convertFrom", Currency.BRL);
        ReflectionTestUtils.setField(currencyConverterRequest, "convertTo", Currency.JPY);
        ReflectionTestUtils.setField(currencyConverterRequest, "value", BigDecimal.valueOf(10.50));

        MvcResult mvcResult = mockMvc.perform(
                MockMvcRequestBuilders.post("/currency_converter")
                        .content(objectMapper.writeValueAsString(currencyConverterRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andReturn();

        Map<String, Object> response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), Map.class);

        assertThat(response)
                .isNotEmpty()
                .hasFieldOrPropertyWithValue("user_id", 1)
                .hasFieldOrPropertyWithValue("from", "BRL")
                .hasFieldOrPropertyWithValue("to", "JPY")
                .hasFieldOrPropertyWithValue("value", 10.50)
                .hasFieldOrPropertyWithValue("converted_value", 225.0618405)
                .hasFieldOrPropertyWithValue("conversion_rate", 21.434461)
                .extractingByKeys("transaction_id", "transaction_date")
                .doesNotContainNull();

        Transaction transaction = transactionRepository.findById(Long.valueOf(response.get("transaction_id").toString())).orElseThrow();
        assertThat(transaction)
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .returns(1L, from(Transaction::getUserId))
                .returns(Currency.BRL, from(Transaction::getFrom))
                .returns(Currency.JPY, from(Transaction::getTo))
                .returns(BigDecimal.valueOf(10.50).setScale(2, RoundingMode.DOWN), from(transaction1 -> transaction1.getValue().setScale(2, RoundingMode.DOWN)))
                .returns(BigDecimal.valueOf(21.434461), from(Transaction::getConversionRate));
    }

    @DisplayName("Should not convert currency because value is negative")
    @Test
    void negativeValue() throws Exception {

        CurrencyConverterRequest currencyConverterRequest = new CurrencyConverterRequest();
        ReflectionTestUtils.setField(currencyConverterRequest, "userId", 1L);
        ReflectionTestUtils.setField(currencyConverterRequest, "convertFrom", Currency.BRL);
        ReflectionTestUtils.setField(currencyConverterRequest, "convertTo", Currency.JPY);
        ReflectionTestUtils.setField(currencyConverterRequest, "value", BigDecimal.valueOf(-1));

        mockMvc.perform(
                MockMvcRequestBuilders.post("/currency_converter")
                        .content(objectMapper.writeValueAsString(currencyConverterRequest))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("errors.[0]", Matchers.equalTo("value: must be greater or equal than 0.01")));
    }

    @DisplayName("Should not convert currency because currency doesn't exists")
    @Test
    void wrongCurrencySymbol() throws Exception {
        Map<Object, Object> body = new HashMap<>();
        body.put("user_id", 1L);
        body.put("convert_from", "ABC");
        body.put("convert_to", Currency.JPY);
        body.put("value", BigDecimal.valueOf(10));

        mockMvc.perform(
                MockMvcRequestBuilders.post("/currency_converter")
                        .content(objectMapper.writeValueAsString(body))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("message", Matchers.containsString("not one of the values accepted for Enum class: [EUR, JPY, BRL, USD]")));
    }

    @DisplayName("Should not convert currency because currency is null")
    @Test
    void nullCurrencySymbol() throws Exception {
        Map<Object, Object> body = new HashMap<>();
        body.put("user_id", 1L);
        body.put("convert_from", Currency.USD);
        body.put("convert_to", null);
        body.put("value", BigDecimal.valueOf(10));

        mockMvc.perform(
                MockMvcRequestBuilders.post("/currency_converter")
                        .content(objectMapper.writeValueAsString(body))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("errors.[0]", Matchers.equalTo("convertTo: must not be null")));
    }

    @DisplayName("Should not convert currency exchange API is down")
    @Test
    void exchangeApiDown() throws Exception {
        Map<Object, Object> body = new HashMap<>();
        body.put("user_id", 1L);
        body.put("convert_from", Currency.USD);
        body.put("convert_to", Currency.BRL);
        body.put("value", BigDecimal.valueOf(10));

        mockMvc.perform(
                MockMvcRequestBuilders.post("/currency_converter")
                        .content(objectMapper.writeValueAsString(body))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is5xxServerError())
                .andExpect(MockMvcResultMatchers.jsonPath("status", Matchers.equalTo("SERVICE_UNAVAILABLE")));
    }


}