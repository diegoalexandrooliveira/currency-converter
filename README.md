# Currency converter

## Acessar a aplicação

O projeto está disponível no seguinte endereço: https://diegoalexandro-currency.herokuapp.com/

Acessar a documentação da API: [Swagger](https://diegoalexandro-currency.herokuapp.com/swagger-ui/index.html)

## Rodar o projeto localmente

Após clonar o repositório, abre o projeto com uma IDE, e configure as variáveis de ambiente.

Como o projeto realiza integração com API externa, é necessario informar uma API Key. É possível usar a integração com o [ExchangeRatesAPI](https://exchangeratesapi.io) ou [FreeCurrencyConverter (default)](https://www.currencyconverterapi.com/).

Rodando a aplicação com o **ExchangeRatesAPI**:

- Crie uma variável de ambiente com o nome EXCHANGE_RATE_API_KEY e a sua key
- Crie uma variável de ambiente com o nome EXCHANGE_API e o valor exchangeRate

Exemplo:
![Exemplo](./readme_images/exchange_rate_config.jpg)

Rodando a aplicação com o **FreeCurrencyConverter**:

- Crie uma variável de ambiente com o nome CURRENCY_CONVERTER_API_KEY e a sua key

Exemplo:
![Exemplo](./readme_images/free_currency_converter_config.jpg)

## Sobre o projeto

Este projeto é uma API Rest que visa prover uma interface que permite a conversão de moedas.

As funcionalidades disponíveis são:

- Converter um valor monetário entre duas moedas, por exemplo, converter 10 reais para dólar.
- Consultar as conversões realizadas por usuário.

O projeto foi construído com a linguagem de programação Java (versão 11) e usa o framework Spring Boot, onde podemos destacar os módulos Spring Web e Spring Data.

A escolha da linguagem e do framework foi definida pela familiaridade com estas tecnologias. O Spring Web nos permite um “start” rápido no projeto, já que não precisamos nos preocupar com os detalhes e focar nas regras e funcionalidades. 

Os dados são persistidos em um banco de dados embarcado chamado H2, e estamos usando a configuração para manter os dados apenas em memória. Mas como o Spring Data cria uma abstração entre a aplicação e banco de dados, podemos facilmente substituir por um banco de dados diferente se fizer sentido para o projeto.

## Organização dos pacotes

Este projeto está organizado em domínios de negocio e dentro de cada domínio temos a separação por caracteristicas das classes.

Exemplo:

- O pacote *transaction* possui todas classes referentes a uma transação, dentro deste pacote temos outros pacotes onde separam as classes de regra de negócio (*domain*) e as da aplicação (*application*).
  ![Pacote transaction](./readme_images/transaction_package.jpg)






