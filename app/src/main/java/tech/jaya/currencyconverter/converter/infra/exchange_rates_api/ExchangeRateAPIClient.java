package tech.jaya.currencyconverter.converter.infra.exchange_rates_api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "exchangeRates", url = "${exchangeRate.url}")
public interface ExchangeRateAPIClient {

    @GetMapping(path = "/latest?access_key=${exchangeRate.key}")
    ExchangeRateResponse getLatest(@RequestParam(name = "base") String currency);

}
