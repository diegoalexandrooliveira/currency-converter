package tech.jaya.currencyconverter.converter.domain;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tech.jaya.currencyconverter.transaction.domain.Transaction;

@Service
@RequiredArgsConstructor
@Slf4j
public class ConvertCurrencyService {

    private final CurrencyRateService currencyRateService;

    public Transaction convert(ConversionRequest conversionRequest) {
        log.debug("Calling CurrencyRateService: {}", conversionRequest);
        var rate = currencyRateService.getRate(conversionRequest.getConvertFrom(), conversionRequest.getConvertTo());

        log.debug("Creating a Transaction. Rate found: {}", rate);
        return new Transaction(conversionRequest.getUserId(), conversionRequest.getConvertFrom(), conversionRequest.getConvertTo(), conversionRequest.getValue(), rate);
    }

}
