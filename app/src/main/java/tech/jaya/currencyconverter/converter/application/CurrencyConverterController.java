package tech.jaya.currencyconverter.converter.application;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.jaya.currencyconverter.converter.domain.ConvertCurrencyService;
import tech.jaya.currencyconverter.transaction.application.TransactionResponse;
import tech.jaya.currencyconverter.transaction.domain.TransactionRepository;
import tech.jaya.currencyconverter.utils.LogTrace;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/currency_converter")
@RequiredArgsConstructor
@Validated
@Slf4j
@Api(value = "/currency_converter", tags = "Conversion operations")
public class CurrencyConverterController {

    private final TransactionRepository transactionRepository;
    private final ConvertCurrencyService convertCurrencyService;

    @PostMapping
    @ApiOperation(value = "Request a value conversion between two currencies")
    public ResponseEntity<TransactionResponse> currencyConverter(@Valid @RequestBody @ApiParam("Currency conversion request") CurrencyConverterRequest request) {
        LogTrace.traceLog();
        log.info("Starting request for conversion. From {} to {}, value {}", request.getConvertFrom(), request.getConvertTo(), request.getValue());
        var transactionRequest = convertCurrencyService.convert(request.createConversionRequest());

        log.debug("Saving transaction. From {} to {}, value {} and rate {}", transactionRequest.getFrom(), transactionRequest.getTo(), transactionRequest.getValue(), transactionRequest.getConversionRate());
        var transactionSaved = transactionRepository.save(transactionRequest);

        return ResponseEntity.ok(TransactionResponse.of(transactionSaved));
    }
}
