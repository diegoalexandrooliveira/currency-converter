package tech.jaya.currencyconverter.converter.infra.currency_converter_api;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;
import tech.jaya.currencyconverter.converter.domain.CurrencyRateService;
import tech.jaya.currencyconverter.exceptions.RateNotFoundException;
import tech.jaya.currencyconverter.transaction.domain.Currency;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@ConditionalOnProperty(name = "exchange-rate-api", havingValue = "currencyConverterApi")
@Slf4j
public class CurrencyConverterAPIServiceImpl implements CurrencyRateService {

    private final CurrencyConverterAPIClient currencyConverterAPIClient;

    @Override
    public BigDecimal getRate(Currency currencyFrom, Currency currencyTo) {
        var currencies = String.format("%s_%s", currencyFrom.toString(), currencyTo.toString());

        log.info("Integrating with external API FreeCurrencyConverter");
        Map<String, BigDecimal> response =  currencyConverterAPIClient.convert(currencies);

        return Optional.ofNullable(response)
                .map(responseMap -> responseMap.get(currencies))
                .orElseThrow(() -> {
                    log.error("Currency not found on response. Response: {}", response);
                    return new RateNotFoundException("Currency not found on API response.");
                });
    }
}
