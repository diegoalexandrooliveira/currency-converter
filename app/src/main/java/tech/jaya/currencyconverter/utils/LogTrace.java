package tech.jaya.currencyconverter.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.slf4j.MDC;

import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LogTrace {

    public static void traceLog() {
        MDC.put("tracing.id", UUID.randomUUID().toString());
    }
}
