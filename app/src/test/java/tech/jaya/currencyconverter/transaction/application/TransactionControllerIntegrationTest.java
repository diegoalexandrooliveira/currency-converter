package tech.jaya.currencyconverter.transaction.application;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import tech.jaya.currencyconverter.transaction.domain.Currency;
import tech.jaya.currencyconverter.transaction.domain.Transaction;
import tech.jaya.currencyconverter.transaction.domain.TransactionRepository;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@AutoConfigureMockMvc
class TransactionControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TransactionRepository transactionRepository;

    private final ObjectMapper objectMapper = new ObjectMapper();


    @DisplayName("Should get a Transaction")
    @Test
    void convertBRLToJPY() throws Exception {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        transactionRepository.deleteAll();
        Transaction transactionSaved = transactionRepository.save(new Transaction(1L, Currency.BRL, Currency.USD, BigDecimal.valueOf(10), BigDecimal.valueOf(5.11)));

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/transactions/1"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andReturn();

        List<Map<String, Object>> response = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), List.class);

        String transactionDate = transactionSaved.getTransactionDate().truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);

        assertThat(response)
                .isNotEmpty()
                .hasSize(1)
                .element(0)
                .hasFieldOrPropertyWithValue("transaction_id", transactionSaved.getId().intValue())
                .returns(transactionDate,
                        res -> ZonedDateTime.parse(res.get("transaction_date").toString(), DateTimeFormatter.ISO_OFFSET_DATE_TIME)
                                .truncatedTo(ChronoUnit.SECONDS)
                                .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
                .hasFieldOrPropertyWithValue("user_id", 1)
                .hasFieldOrPropertyWithValue("from", "BRL")
                .hasFieldOrPropertyWithValue("to", "USD")
                .hasFieldOrPropertyWithValue("value", 10.0)
                .hasFieldOrPropertyWithValue("converted_value", 51.10)
                .hasFieldOrPropertyWithValue("conversion_rate", 5.11);
    }

    @DisplayName("Should not return a Transaction")
    @Test
    void shouldNotReturn() throws Exception {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        transactionRepository.deleteAll();
        transactionRepository.save(new Transaction(1L, Currency.BRL, Currency.USD, BigDecimal.valueOf(10), BigDecimal.valueOf(5.11)));

        mockMvc.perform(MockMvcRequestBuilders.get("/transactions/2"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.content().json("[]"));

    }
}