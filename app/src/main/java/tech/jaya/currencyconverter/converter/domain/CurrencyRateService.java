package tech.jaya.currencyconverter.converter.domain;

import tech.jaya.currencyconverter.transaction.domain.Currency;

import java.math.BigDecimal;

public interface CurrencyRateService {

    BigDecimal getRate(Currency currencyFrom, Currency currencyTo);

}
