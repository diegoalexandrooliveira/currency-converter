package tech.jaya.currencyconverter.converter.domain;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import org.springframework.util.Assert;
import tech.jaya.currencyconverter.transaction.domain.Currency;

import java.math.BigDecimal;

@Getter
@ToString
public class ConversionRequest {
    private Long userId;

    private Currency convertFrom;

    private Currency convertTo;

    private BigDecimal value;

    public ConversionRequest(@NonNull Long userId, @NonNull Currency convertFrom, @NonNull Currency convertTo, @NonNull BigDecimal value) {
        Assert.state(value.compareTo(BigDecimal.valueOf(0.01)) >= 0, "Value must be greater or equal than 0.01");
        this.userId = userId;
        this.convertFrom = convertFrom;
        this.convertTo = convertTo;
        this.value = value;
    }
}
