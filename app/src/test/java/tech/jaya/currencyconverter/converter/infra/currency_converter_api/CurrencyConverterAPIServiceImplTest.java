package tech.jaya.currencyconverter.converter.infra.currency_converter_api;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import tech.jaya.currencyconverter.exceptions.RateNotFoundException;
import tech.jaya.currencyconverter.transaction.domain.Currency;

import java.math.BigDecimal;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class CurrencyConverterAPIServiceImplTest {

    private CurrencyConverterAPIClient currencyConverterAPIClient;

    private CurrencyConverterAPIServiceImpl currencyConverterAPIService;

    @BeforeEach
    void setup() {
        currencyConverterAPIClient = mock(CurrencyConverterAPIClient.class);
        currencyConverterAPIService = new CurrencyConverterAPIServiceImpl(currencyConverterAPIClient);
    }

    @DisplayName("Should get exchange rate")
    @Test
    void shouldGetRate() {
        when(currencyConverterAPIClient.convert("JPY_EUR")).thenReturn(Map.of("JPY_EUR", BigDecimal.valueOf(0.005)));
        BigDecimal rate = currencyConverterAPIService.getRate(Currency.JPY, Currency.EUR);

        assertThat(rate)
                .isEqualTo(BigDecimal.valueOf(0.005));
    }

    @DisplayName("Should throw RateNotFoundException")
    @ParameterizedTest
    @NullAndEmptySource
    void shouldThrowException(Map<String, BigDecimal> response) {
        when(currencyConverterAPIClient.convert("JPY_EUR")).thenReturn(response);
        assertThatExceptionOfType(RateNotFoundException.class)
                .isThrownBy(() -> currencyConverterAPIService.getRate(Currency.JPY, Currency.EUR));
    }


}