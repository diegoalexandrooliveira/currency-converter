package tech.jaya.currencyconverter.transaction.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class TransactionTest {

    @DisplayName("Should create a transaction with date time")
    @Test
    void createTransaction() {
        Transaction transaction = new Transaction(1L, Currency.BRL, Currency.USD, BigDecimal.ONE, BigDecimal.ONE);
        assertThat(transaction).isNotNull();
        ZonedDateTime transactionDate = transaction.getTransactionDate();
        assertThat(transactionDate).isNotNull();
    }

    @DisplayName("Should convert value")
    @Test
    void convertValue() {
        Transaction transaction = new Transaction(1L, Currency.BRL, Currency.USD, BigDecimal.valueOf(50), BigDecimal.valueOf(2L));
        assertThat(transaction).isNotNull();
        BigDecimal convertedValue = transaction.convertValue();

        assertThat(convertedValue)
                .isNotNull()
                .isEqualTo(BigDecimal.valueOf(100));
    }

    @DisplayName("Should not create a transaction when userId is null")
    @Test
    void userIdNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> new Transaction(null, Currency.BRL, Currency.USD, BigDecimal.ONE, BigDecimal.ONE));
    }

    @DisplayName("Should not create a transaction when from is null")
    @Test
    void fromNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> new Transaction(1L, null, Currency.USD, BigDecimal.ONE, BigDecimal.ONE));
    }

    @DisplayName("Should not create a transaction when to is null")
    @Test
    void toNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> new Transaction(1L, Currency.BRL, null, BigDecimal.ONE, BigDecimal.ONE));
    }

    @DisplayName("Should not create a transaction when value is null")
    @Test
    void valueNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> new Transaction(1L, Currency.BRL, Currency.USD, null, BigDecimal.ONE));
    }

    @DisplayName("Should not create a transaction when conversionRate is null")
    @Test
    void rateNull() {
        assertThatExceptionOfType(NullPointerException.class).isThrownBy(() -> new Transaction(1L, Currency.BRL, Currency.USD, BigDecimal.ONE, null));
    }

}