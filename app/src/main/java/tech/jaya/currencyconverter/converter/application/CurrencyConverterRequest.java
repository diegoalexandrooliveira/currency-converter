package tech.jaya.currencyconverter.converter.application;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import tech.jaya.currencyconverter.converter.domain.ConversionRequest;
import tech.jaya.currencyconverter.transaction.domain.Currency;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@ToString
public class CurrencyConverterRequest {
    @JsonProperty("user_id")
    @NotNull
    private Long userId;

    @JsonProperty("convert_from")
    @NotNull
    private Currency convertFrom;

    @JsonProperty("convert_to")
    @NotNull
    private Currency convertTo;

    @NotNull
    @DecimalMin(value = "0.01", message = "must be greater or equal than 0.01")
    private BigDecimal value;

    public ConversionRequest createConversionRequest() {
        return new ConversionRequest(userId, convertFrom, convertTo, value);
    }
}
