package tech.jaya.currencyconverter.converter.domain;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import tech.jaya.currencyconverter.transaction.domain.Currency;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.*;

class ConversionRequestTest {

    @DisplayName("Should create a ConversionRequest")
    @Test
    void create() {
        ConversionRequest conversionRequest = new ConversionRequest(1L, Currency.BRL, Currency.JPY, BigDecimal.TEN);

        assertThat(conversionRequest)
                .isNotNull();
    }

    @DisplayName("Should throw NullPointerException with null values")
    @Test
    void nullValues() {
        assertThatNullPointerException().isThrownBy(() -> new ConversionRequest(1L, null, Currency.JPY, BigDecimal.TEN));
    }

    @DisplayName("Should not create a ConversionRequest when value is below 0.01")
    @ParameterizedTest
    @ValueSource(doubles = {0, 0.009, -1, -1000})
    void valueBelowZero(double value) {
        BigDecimal bigDecimalValue = BigDecimal.valueOf(value);
        assertThatThrownBy(() -> new ConversionRequest(1L, Currency.BRL, Currency.JPY, bigDecimalValue))
                .isInstanceOf(IllegalStateException.class)
                .hasMessage("Value must be greater or equal than 0.01");
    }

}