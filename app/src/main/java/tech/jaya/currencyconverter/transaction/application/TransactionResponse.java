package tech.jaya.currencyconverter.transaction.application;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import tech.jaya.currencyconverter.transaction.domain.Currency;
import tech.jaya.currencyconverter.transaction.domain.Transaction;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Builder(access = AccessLevel.PRIVATE)
@Getter
public class TransactionResponse {

    @JsonProperty("transaction_id")
    private final Long id;

    @JsonProperty("user_id")
    private final Long userId;

    private final Currency from;

    @JsonProperty("value")
    private final BigDecimal value;

    private final Currency to;

    @JsonProperty("converted_value")
    private final BigDecimal convertedValue;

    @JsonProperty("conversion_rate")
    private final BigDecimal conversionRate;

    @JsonProperty("transaction_date")
    private final ZonedDateTime transactionDate;


    public static TransactionResponse of(Transaction transaction) {
        return TransactionResponse.builder()
                .id(transaction.getId())
                .userId(transaction.getUserId())
                .from(transaction.getFrom())
                .value(transaction.getValue())
                .convertedValue(transaction.convertValue())
                .to(transaction.getTo())
                .conversionRate(transaction.getConversionRate())
                .transactionDate(transaction.getTransactionDate())
                .build();
    }


}
