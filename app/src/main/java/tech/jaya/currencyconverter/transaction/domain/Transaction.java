package tech.jaya.currencyconverter.transaction.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Entity
@Table(name = "transactions")
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Transaction {

    @Id
    @GeneratedValue(generator = "transaction_sequence", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "transaction_sequence", sequenceName = "transaction_sequence", allocationSize = 1)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Enumerated(EnumType.STRING)
    @Column(name = "convert_from")
    private Currency from;

    @Enumerated(EnumType.STRING)
    @Column(name = "convert_to")
    private Currency to;

    private BigDecimal value;

    @Column(name = "conversion_rate")
    private BigDecimal conversionRate;

    @Column(name = "transaction_date")
    private ZonedDateTime transactionDate;

    public Transaction(@NonNull Long userId, @NonNull Currency from, @NonNull Currency to, @NonNull BigDecimal value, @NonNull BigDecimal conversionRate) {
        this.userId = userId;
        this.from = from;
        this.to = to;
        this.value = value;
        this.conversionRate = conversionRate;
        this.transactionDate = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("UTC"));
    }

    public BigDecimal convertValue(){
        return value.multiply(conversionRate);
    }
}
