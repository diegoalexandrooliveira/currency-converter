CREATE TABLE transactions (
   id BIGINT NOT NULL PRIMARY KEY,
   user_id BIGINT NOT NULL,
   convert_from VARCHAR(10) NOT NULL,
   convert_to VARCHAR(10) NOT NULL,
   value DECIMAL(20,2) NOT NULL,
   conversion_rate DECIMAL(30,6) NOT NULL,
   transaction_date TIMESTAMP WITH TIME ZONE
);

CREATE SEQUENCE transaction_sequence;