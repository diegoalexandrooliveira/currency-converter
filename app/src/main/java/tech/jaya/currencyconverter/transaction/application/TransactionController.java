package tech.jaya.currencyconverter.transaction.application;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tech.jaya.currencyconverter.transaction.domain.TransactionRepository;
import tech.jaya.currencyconverter.utils.LogTrace;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/transactions")
@RequiredArgsConstructor
@Slf4j
@Api(value = "/transactions", tags = "Transaction operations")
public class TransactionController {

    private final TransactionRepository transactionRepository;

    @GetMapping(path = "/{user_id}")
    @ApiOperation(value = "List all request of currency conversion filtering by user identification.")
    public ResponseEntity<List<TransactionResponse>> getTransactions(@PathVariable("user_id") @ApiParam("User identification") Long userId) {
        LogTrace.traceLog();
        log.info("Searching transactions of user {}", userId);
        List<TransactionResponse> transactions = transactionRepository
                .findByUserIdOrderById(userId)
                .stream()
                .map(TransactionResponse::of)
                .collect(Collectors.toList());

        return ResponseEntity.ok(transactions);
    }
}
