package tech.jaya.currencyconverter.converter.infra.exchange_rates_api;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public class ExchangeRateResponse {
    private boolean success;
    private Map<String, BigDecimal> rates;
    private Map<Object, Object> error;
}
