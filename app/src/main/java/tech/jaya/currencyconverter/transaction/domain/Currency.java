package tech.jaya.currencyconverter.transaction.domain;

public enum Currency {
    BRL,
    USD,
    EUR,
    JPY
}
